const mongoose = require('mongoose');
const Course = require('../Models/coursesSchema.js');
const jwt = require('jsonwebtoken');
const auth = require('../auth.js');

// Create a new course
/*
	Steps:
		1. Create a new Course object using the mongoose model and the information from the equest body and the id from the header.
		2. Save the new User to the database.
*/

module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){
		const input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// Save function
		return newCourse.save()
		.then(course => {
			console.log(course)
			response.send(`The course ${course.name} has been created.`);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	} else {
		return response.send(`You are not authorized to create a course.`)
	}
	
};