const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


// Controllers


// This controller will create/register a user in our database.
module.exports.userRegistration = (request, response) => {

	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {

		if(result !== null){
			return response.send("A user with this email already exists!");
		} else {
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send("You are now registered. Thank you!");
			})
			.catch(error => {
				return response.send(error);
			});
			
		}
	})
	.catch(error => {
		return response.send(error)
	});
};

// User Authentication
module.exports.userAuthentication = (request, response) => {

	let input = request.body;

	/*
		Possible scenarios:
			1. Email is not yet registered
			2. Email is registered, but the password is wrong
	*/

	User.findOne({email: input.email})
	.then(result => {

		// Email registration check
		if(result === null){
			return response.send("Email is not yet registered. Please register before logging in.");
		} else {

			// Password verification
			// .CompareSync() is used to compare a non-encrypted password to the encrypted password. It returns a boolean value: true if they match, false if not.

			const isPasswordCorrect = bcrypt.compareSync(input.password,result.password);

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect.");
			}

		}
	})
	.catch(error => {
		return response.send(error);
	})

};

// getUser Controller
module.exports.getUser = (request, response) => {

	// const input = request.body;
	const userData = auth.decode(request.headers.authorization);
	// console.log(userData);

	return User.findById(userData._id)
	.then(result => {
		result.password = "";
		return response.send(result);
	})

	/*User.findById(input.id)
	.then(result => {

		if(result === null){
			return response.send("User not found.");
		} else {
			result.password = ""
			return response.send(result);
		}
	})*/
};